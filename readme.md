## miros学习项目

#### 1.项目来源：

课程原版资料：https://www.state-machine.com/video-course，	网站包含视频、视频文字、源代码等资料；

b站视频：https://www.bilibili.com/video/BV1j3411B7KK，	英文无字幕；

`miros`主要学习课程为`lesson22`~`lesson28`;

#### 2.硬件：

`STM32F103RBT6`线路板，其中`PA2-LED1`，`PA3-LED2`；可根据实际情况自行准备对应硬件，建议使用`Cotex-M3`内核的开发板方便移植`PendSV_Handler`中的汇编代码。本项目采用`HAL`库进行开发；

#### 3.课程内容：

##### lesson22：理解rtos内核中任务切换

整个试验过程如下：

> 1.创建2个简单可识别的thread，灯1、灯2的闪烁；
>
> 2.在SysTick_Handler结束时打断点；
>
> 3.通过register记录sp的值，并在memory中找到sp后，地址向下依次为：R0/R1/R2/R3/R12/LR/PC/xPSR；
>
> 4.在disassembly窗口中找到需要下一个执行thread的pc值，更改memory中的PC寄存器的值，点击单步调试，即可跳转到另一个thread；
>
> 以上4步是最原始的手动切换thread，接下来是用sp指针来切换thread；
>
> 1.定义栈数组和栈指针，并将thread的地址赋值到栈数组中；
>
> 2.在SysTick_Handler结束时打断点；
>
> 3.在registe中将sp的值更改为thread所对应的指针的地址（watch中查看）；
>
> 4.选中disassembly窗口后点击单步调试，即可跳转到另一个thread；

