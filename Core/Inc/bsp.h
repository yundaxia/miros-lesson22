#ifndef __BSP_H
#define __BSP_H

#include "stdint.h"
#include "stm32f1xx_hal.h"

/* system clock tick [Hz] */
#define BSP_TICKS_PER_SEC   1000U

#define LED1_PORT   GPIOA
#define LED1_PIN    GPIO_PIN_2

#define LED2_PORT   GPIOA
#define LED2_PIN    GPIO_PIN_3

void bsp_delay(uint32_t ticks);

extern void bsp_led1_on(void);
extern void bsp_led1_off(void);
extern void bsp_led2_on(void);
extern void bsp_led2_off(void);



#endif
