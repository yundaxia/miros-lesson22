#include "bsp.h"


static uint32_t volatile global_cnt;

/*
 * @brief  systick中断处理函数
 * @param  
 * @retval 
 * @attention 
 */
void HAL_IncTick(void)
{
    ++global_cnt;
}

uint32_t bsp_ticks(void)
{
    uint32_t ticks;

    __disable_irq();
    ticks = global_cnt;
    __enable_irq();

    return ticks;
}

void bsp_delay(uint32_t ticks)
{
    uint32_t start = bsp_ticks();

    while ((bsp_ticks() - start) < ticks)
    {
        /* code */
    }
}

void bsp_led1_on(void)
{
    HAL_GPIO_WritePin(LED1_PORT, LED1_PIN, GPIO_PIN_SET);
}

void bsp_led1_off(void)
{
    HAL_GPIO_WritePin(LED1_PORT, LED1_PIN, GPIO_PIN_RESET);
}

void bsp_led2_on(void)
{
    HAL_GPIO_WritePin(LED2_PORT, LED2_PIN, GPIO_PIN_SET);
}

void bsp_led2_off(void)
{
    HAL_GPIO_WritePin(LED2_PORT, LED2_PIN, GPIO_PIN_RESET);
}

